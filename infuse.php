<?php
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: *");
    header('Access-Control-Allow-Credentials: true');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:{$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

function addVisitor()
{
    try {
        checkVisit();
        $link = getLink();
        $data = buildData();
        if ($data['views'] > 0) {
            $query = mysqli_prepare($link, 'UPDATE visitors SET views_count = ?, view_date = ? WHERE page_url = ?');
            $viewsCount = $data['views'] + 1;
            $query->bind_param("sss", $viewsCount, $data['time'], $_GET['page']);
        } else {
            $query = mysqli_prepare($link, 'INSERT INTO visitors VALUES (?, ?, ?, ?, ?)');
            $views = 1;
            $query->bind_param("ssssi", $data['ip'], $data['userAgent'], $data['time'], $data['pageUrl'], $views);
        }

        $query->execute();

        return './images/image.jpg';
    } catch (\Exception $e) {
        return $e->getMessage();
    }
}

function buildData()
{
    return [
        'ip' => $_SERVER['REMOTE_ADDR'],
        'userAgent' => $_SERVER['HTTP_USER_AGENT'],
        'time' => time(),
        'pageUrl' => $_GET['page'],
        'views' => checkVisit()
    ];
}

function getLink()
{
    return mysqli_connect("127.0.0.1", "root", "", "infusemedia", 3306);
}

function checkVisit()
{
    try {
        $link = getLink();
        $query = mysqli_prepare($link, 'SELECT * FROM visitors WHERE ip_address = ? AND user_agent = ? AND page_url = ?');
        $query->bind_param('sss', $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT'], $_GET['page']);
        $query->execute();
        $result = $query->get_result();

        if ($result->num_rows > 0) {
            return $result->fetch_assoc()['views_count'];
        }

        return 0;
    } catch (\Exception $exception) {
        return $exception->getMessage();
    }
}

addVisitor();